<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Knowledge extends Hook
{
    public $actions = [
        'hide',
        'discover',
        'deliver',
    ];

    public static $prefixes = [
        'location',
        'secrets',
        'whereabouts',
    ];


    public function __construct()
    {
        parent::__construct($this->actions);
    }
}
