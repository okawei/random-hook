<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Hook implements GeneratesNames
{
    public  $actions = [
        'attend',
        'ruin',
        'stop',
        'find',
        'gather supplies for',
        '{chain}'
    ];


    public static $chainActions = [
        'deliver the {entity} to {name}' => [
            Item::class
        ],
        'see to it that {entity} does not ruin the {name}' => [
            Item::class,
            Person::class
        ],
        'sacrifice {entity} at the {name}' => [
            Person::class
        ]
    ];


    public static function getChain(){
        $key = array_rand(self::$chainActions);
        return [$key, self::$chainActions[$key]];
    }



    public function __construct()
    {
        parent::__construct($this->actions);
    }

    public static $modifier = [
        'Fall',
        'Winter',
        'Spring',
        'Summer',
        'containment',
        'ascension',
    ];

    public static $prefix = [
        'grand',
        'great',
        'looming',
        'sacred',
        'accursed',
        'pitiful',
        'annual',
        'semi-annual',
        'weekly',
        'holy'
    ];

    public static $type = [
        'festival',
        'harvest',
        'ball',
        'soiree',
        'party',
        'dance',
        'sacrifice',
        'ritual',
        'gathering',
    ];


    public static function generateName(): string
    {
        $prefix = '';
        if (rand(0, 1)){
            $prefix = self::$prefix[array_rand(self::$prefix)];
        }
        $prefix = $prefix.' ';
        if (rand(0, 1)){
            return $prefix.self::$modifier[array_rand(self::$modifier)].' '.self::$type[array_rand(self::$type)];
        } else {
            return $prefix.self::$type[array_rand(self::$type)]. ' of '.self::$modifier[array_rand(self::$modifier)];
        }
    }
}
