<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Hook extends Model
{
    public $actions;

    public function __construct($actions)
    {
        $this->actions = $actions;
        parent::__construct([]);
    }

    public function getAction(){
        return $this->actions[array_rand($this->actions)];
    }

}
