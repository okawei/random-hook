<?php

namespace App\Http\Controllers;

use App\Event;
use App\Hook;
use App\Item;
use App\Knowledge;
use App\Location;
use App\Person;
use App\Services\RandomItemNameService;
use Faker\Generator;
use Illuminate\Http\Request;

class LocationHookController extends Controller
{

    public function generateLocationHook(){
        $adventure = Location::generatePrefix();
        $possibleHooks = [
            Person::class,
            Event::class,
            Item::class,
            Knowledge::class
        ];
        /** @var Hook $hookClass */
        $hookClass = new $possibleHooks[array_rand($possibleHooks)]();
        $action = $hookClass->getAction();
        if (get_class($hookClass) === Event::class){
            $adventure = $this->handleEvent($action, $adventure);
        } else if (get_class($hookClass) === Item::class) {
            $adventure = $this->handleItem($adventure, $action);
        } else if (get_class($hookClass) === Knowledge::class){
            $adventure = $this->handleKnowledge($action, $adventure);
        } else {
            $adventure = $this->handlePerson($action, $adventure);
        }
        $adventure = $this->replaceInlineEntities($adventure);
        $adventure.='.';
        return view('location-hook', compact('adventure'));
    }

    /**
     * @param string $adventure
     * @param $action
     * @return string
     */
    public function handleItem(string $adventure, $action): string
    {
        if ($action === '{chain}') {
            $class = Item::class;
            $adventure = $this->handleChain($adventure, $action, $class);
        } else {
            $adventure .= ' '.$action.' the ' . Item::generateName();
        }


        return $adventure;
    }

    /**
     * @param $action
     * @param string $adventure
     * @return string
     */
    public function handleEvent($action, string $adventure): string
    {
        if ($action === '{chain}') {
            $class = Event::class;
            $adventure = $this->handleChain($adventure, $action, $class);
        } else {
            $adventure .= $action . ' the ' . Event::generateName();
        }
        return $adventure;
    }

    /**
     * @param $action
     * @param string $adventure
     * @return string
     */
    public function handleKnowledge($action, string $adventure): string
    {
        $knowlegeOf = [
            Person::class,
            Item::class,
            Event::class
        ][rand(0, 2)];

        $knowledgeModifier = Knowledge::$prefixes[array_rand(Knowledge::$prefixes)] . ' of ';

        if ($knowlegeOf == Person::class) {
            $knowledgeModifier .= Person::generateName();
        } else if ($knowlegeOf === Item::class) {
            $knowledgeModifier .= 'the ' . Item::generateName();
        } else if ($knowlegeOf === Event::class) {
            $knowledgeModifier .= 'the ' . Event::generateName();
        }
        $adventure .= $action . ' the ' . $knowledgeModifier;
        return $adventure;
    }

    /**
     * @param $action
     * @param string $adventure
     * @return string
     */
    public function handlePerson($action, string $adventure): string
    {
        if ($action == '{chain}'){
            $class = Person::class;
            $adventure = $this->handleChain($adventure, $action, $class);
        } else {
            $adventure .= $action . ' ' . Person::generateName();
        }

        return $adventure;
    }

    /**
     * @param string $adventure
     * @return mixed|string
     */
    public function replaceInlineEntities(string $adventure)
    {
        $adventure = str_replace('{location}', Location::generateName(), $adventure);
        $adventure = str_replace('{person}', Person::generateName(), $adventure);
        $adventure = str_replace('{item}', Item::generateName(), $adventure);
        return $adventure;
    }

    /**
     * @param string $adventure
     * @param $action
     * @param string $class
     * @return string
     */
    public function handleChain(string $adventure, $action, string $class): string
    {
        list($action, $availableClasses) = $class::getChain();
        $name = $class::generateName();
        $entity = $availableClasses[array_rand($availableClasses)];
        if ($entity === Item::class){
            $entity = ' the '.$entity::generateName();
        } else {
            $entity = $entity::generateName();
        }
        $action = str_replace('{name}', $name, $action);
        $action = str_replace('{entity}', $entity, $action);

        $adventure .= ' ' . $action;
        return $adventure;
    }
}
