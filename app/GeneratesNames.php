<?php
namespace App;

interface GeneratesNames{
    public static function generateName(): string;
}
