<?php

namespace App;

use Faker\Generator;
use Illuminate\Database\Eloquent\Model;

class Person extends Hook implements GeneratesNames
{
    public $actions = [
        'kill',
        'attend the wedding of',
        'befriend',
        'corrupt',
        '{chain}',
        '{chain}',
        '{chain}',
        '{chain}',
        '{chain}'
    ];

    public function __construct()
    {
        parent::__construct($this->actions);
    }

    public static $chainActions = [
        'find {name}, the daughter of the king' =>[
            Person::class
        ],
        'find {name}, the son of the king' =>[
            Person::class
        ],
        'find {name}, the wife of the king' =>[
            Person::class
        ],
        'find {name}, the husband of the king' =>[
            Person::class
        ],
        'save {name} from {entity}' => [
            Person::class,
            Item::class
        ],
        'work with {name} to find {entity}' => [
            Item::class,
            Location::class
        ],
        'help {name} to find {entity}' => [
            Item::class,
            Location::class
        ],
        'escort {name} to {entity}'=>[
            Location::class
        ],
        'kill {name} with {entity}' => [
            Item::class
        ]
    ];


    public static function generateName(): string
    {
        $faker = app(Generator::class);
        return $faker->name;
    }


    public static function getChain(){
        $key = array_rand(self::$chainActions);
        return [$key, self::$chainActions[$key]];
    }
}
