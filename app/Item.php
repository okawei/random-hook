<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Hook implements GeneratesNames
{
    public function __construct()
    {
        parent::__construct($this->actions);
    }

    public static $modifier = [
        'legend',
        'lore',
        '{location}',
        '{person}',
        'the wizard',
        'the bard',
        'the fighter',
        'the cleric',
        'the warlock',
        'the rogue',
        'spite',
        'hatred',
        'the gods',
        'the devil',
        'angels',
        'murder',
        'sacrifice',
        'doom',
        'destruction'
    ];

    public static $prefix = [
        'grand',
        'great',
        'looming',
        'sacred',
        'accursed',
        'pitiful',
        'holy',
        'burning',
        'freezing',
        'arcane',
        'secret',
        'hidden',
        'explosive',
        'disgusting'
    ];

    public static $type = [
        'ring',
        'necklace',
        'crystal',
        'amulet',
        'tomb',
        'crown',
        'spellbook',
        'dagger',
        'staff',
        'sword',
        'shield',
        'fountain',
        'chalice'
    ];

    public static function generateName(): string{
        $prefix = '';
        if (rand(0, 1)){
            $prefix = self::$prefix[array_rand(self::$prefix)];
        }
        $prefix = $prefix.' ';
        return $prefix.self::$type[array_rand(self::$type)]. ' of '.self::$modifier[array_rand(self::$modifier)];

    }

    public $actions = [
        'destroy',
        'find',
        'steal',
        'create',
        'activate',
        'consecrate',
        '{chain}',
        '{chain}',
        '{chain}',
        '{chain}',
        '{chain}',
        '{chain}',
    ];

    public static $chainActions = [
        'deliver the {name} to {entity}' => [
            Person::class,
        ],
        'deliver the {name} to the {entity}' => [
            Location::class
        ],
        'destroy the {name} for {entity}' => [
            Person::class
        ],
        'kill {entity} with the {name}' => [
            Person::class
        ],
        'save {entity} with the {name}' => [
            Location::class,
            Person::class
        ],
        'destroy {entity} with the {name}' => [
            Location::class
        ],
        'assemble the {name} with the help of {entity}' => [
            Person::class,
            Item::class
        ]
    ];

    public static function getChain(){
        $key = array_rand(self::$chainActions);
        return [$key, self::$chainActions[$key]];
    }

}
