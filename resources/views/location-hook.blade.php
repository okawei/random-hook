@extends('includes.master')

@section('body')
    <div class="hook">
        <img src="/images/hook.png">
        <h1>Random D&D Plot Hook Generator</h1>
        <h2>{{$adventure}}</h2>
        <a class="new-hook" onclick="location.reload()">New Hook</a>
    </div>
@stop
